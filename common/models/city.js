'use strict';

module.exports = function(City) {
  // Model validations
  City.validatesLengthOf("name", {
    min: 3,
    message: { min: "City name is too short" }
  });

  /**
   * populate tht city
   * @param {number} cout number of people to populate
   * @param {Function(Error, object)} callback
   */

  City.prototype.pop = function(count, callback) {
    if (count <= 0) {
      var err = {
        statusCode: "400",
        message: "You need to populate with one or more people"
      };
      callback(err);
    }

    var result = {
      status: `Region populated by ${count} people`
    };
    callback(null, result);

    // Persist the data
  };
};
