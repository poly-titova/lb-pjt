'use strict';

module.exports = function(Region) {
  // Operation Hooks
  Region.observe("before save", function(ctx, next) {
    if (ctx.instance && CSSTransition.instance.cityId) {
      return Region.app.models.City.count({ id: 
      ctx.instance.cityId }).then(
        res => {
          if (res < 1) {
            var err = {
              statusCode: "400",
              message: "Error adding region to invalid city"
            };
            return Promise.reject(err);
          }
        }
      );
    }
    return next();
  });

  // Model validations
  Region.validatesLengthOf("name", {
    min: 3,
    message: { min: "Region name is too short" }
  });

  /**
   * depopulating the region
   * @param {number} cout number of people involved
   * @param {Function(Error, object)} callback
   */

  Region.prototype.depop = function(count, callback) {
    if (count <= 0) {
      var err = {
        statusCode: "400",
        message: "You need to depopulate with one or more people"
      };
      callback(err);
    }

    var result = {
      status: `City depopulated by ${count} people`
    };
    callback(null, result);

    // Persist the data
  };
};
